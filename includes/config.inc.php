<?php
    /** 
     * PHP Error Settings
     */
    // show all errors, but not the notices
    error_reporting( E_ALL & ~E_NOTICE );
    // show errors in the browser
    ini_set( 'display_errors', 1 );
    /** 
     * Timezone Settings
     */
    date_default_timezone_set( 'America/Toronto' );
    /**
     * Site Settings
     */
    define( 'SITE_TITLE',   'HTML Email' );
    define( 'DEFAULT_PAGE', 'index' );
    define( 'URL_REWRITE',  true );
    define( 'SITE_ROOT',    'http://projects.hapsay.com/Email-Newsletter/' );
    /**
     * Database Credentials (change these to your own)
     */
	define( 'DB_HOST',      'localhost' );
    define( 'DB_USER',      'hapsayco_moc' );
    define( 'DB_PASSWORD',  'M0c78922' );
    define( 'DB_NAME',      'hapsayco_mysql' );

	$db = mysqli_connect( DB_HOST, DB_USER, DB_PASSWORD, DB_NAME )
        or die( mysqli_connect_error() );