<?php
	
	require( 'includes/config.inc.php' );
	
	//Jsend assumes you didnt provide email at all
	$jSend = array(
		'status' => 'fail',
		'data' => array(
				'email' => 'Please enter a valid email address.'
			)
	);

	
	if( isset( $_POST[ 'email' ] ) ){
		
		if( !filter_var( $_POST[ 'email' ], FILTER_VALIDATE_EMAIL ) ){
			//do nothing
		
		} else {
			
			$email = mysqli_real_escape_string( $db, strip_tags( trim( $_POST[ 'email' ] ) ) );
			
			$query = "INSERT INTO email_subscribers(email) VALUES('$email')";
			
			$result = mysqli_query( $db, $query ); 
            // or die( mysqli_error( $db ) . '<br>' . $query );
			
			if( $result ){
				
                require( 'includes/PHPMailer/src/PHPMailer.php' );
                require( 'includes/PHPMailer/src/Exception.php' );
                
                $mail = new \PHPMailer\PHPMailer\PHPMailer( true );
                
                try{
                    // send email here
                    
                    $mail->addAddress( $email );
                    $mail->setFrom( 'sharklasers@thomasborzecki.ca', 'Shark Lasers' );
                    $mail->addReplyTo( 'noreply@thomasborzecki.ca' );
                    $mail->Subject = 'Shark Lasers Newsletter';
                    $mail->isHTML( true );
                    
                    $mail->Body = file_get_contents( 'email/email-newsletter.html' );
                    $mail->AltBody = strip_tags( $mail->Body );
                    
                    $mail->send();
                    
                    $jSend[ 'status' ] = 'success';
					$jSend[ 'data' ][ 'email' ] = 'The email was succesfully sent.';
                    
                } catch( Exception $e ){
                    
                    
                    $jSend[ 'status' ] = 'error'; 
					
                    $jSend[ 'data' ] = array(
						'PHPMailer' => $e->getMessage());
			
//					unset( $jSend[ 'data' ] );
					
					$jSend[ 'message' ] = 'Server Problem';
                }
                
			} else {
				$jSend[ 'data' ][ 'email' ] = 'This email is already subscribed to the newsletter.';
			}
		}
	}

header( 'Content-Type:  application/json' );
echo json_encode ($jSend);

