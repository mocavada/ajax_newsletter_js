JSON


[{
	"id" : 1223234343434343434,
	"name" : "Product Name",
	"image" : "images/product-photo.jpg",
	"style" : "Some Style",
	"price" : "$99.99",
	"description" : "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old."
	
},{
	"id" : 1223234343434343434,
	"name" : "Product Name",
	"image" : "images/product-photo.jpg",
	"style" : "Some Style",
	"price" : "$99.99",
	"description" : "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old."

}]


XML
<product-list>
	<product id="213123223321">
		<name>Product Name</name>
		<image>images/product-photo.jpg</image>
		<style>Some Style</style>
		<price>$99.99</price>
		<description>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</description>
	</product>
	<product id="21314334355">
		<name>Product Name</name>
		<image>images/product-photo.jpg</image>
		<style>Some Style</style>
		<price>$99.99</price>
		<description>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</description>
	</product>
</product-list>	