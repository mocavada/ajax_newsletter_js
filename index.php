<?php

	require( 'includes/config.inc.php' );
	
	$errors = array();
	$response = '';
	if( isset( $_POST[ 'email' ] ) ){
		if( !filter_var( $_POST[ 'email' ], FILTER_VALIDATE_EMAIL ) ){
			$errors[ 'email' ] = '<p>Please enter a valid email address.</p>';
		}
		
		if( count( $errors ) == 0 ){
			
			$email = mysqli_real_escape_string( $db, strip_tags( trim( $_POST[ 'email' ] ) ) );
			
			$query = "INSERT INTO email_subscribers(email) VALUES('$email')";
			$result = mysqli_query( $db, $query ); 
            // or die( mysqli_error( $db ) . '<br>' . $query );
			
			if( $result ){
				
                require( 'includes/PHPMailer/src/PHPMailer.php' );
                require( 'includes/PHPMailer/src/Exception.php' );
                
                $mail = new \PHPMailer\PHPMailer\PHPMailer( true );
                
                try{
                    // send email here
                    
                    $mail->addAddress( $email );
                    $mail->setFrom( 'sharklasers@thomasborzecki.ca', 'Shark Lasers' );
                    $mail->addReplyTo( 'noreply@thomasborzecki.ca' );
                    $mail->Subject = 'Shark Lasers Newsletter';
                    $mail->isHTML( true );
                    
                    $mail->Body = file_get_contents( 'email/email-newsletter.html' );
                    $mail->AltBody = strip_tags( $mail->Body );
                    
                    $mail->send();
                    
                    $response = '<p>Email newsletter was sent successfully.</p>';
				    $_POST[ 'email' ] = '';
                    
                } catch( Exception $e ){
                    
                    /* echo '<pre>';
                    print_r( $e );
                    echo '</pre>'; */
                    
                    $errors[ 'email' ] 
                    = '<p>There was a server problem when sending your email. 
                          Please try again later.</p>';
                }
                
			} else {
				$errors[ 'email' ] = '<p class="error">This email is already subscribed to the newsletter. </p>';
			}
		}
	}
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />

        <!-- viewport settings for responsive layouts -->
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        
        <title>Email Newsletter Testing Tool</title>
        
        <!-- main stylesheet link -->
        <link rel="stylesheet" href="css/style.css" />
        
		<!-- JS app-->
		<script src="js/app.js"></script>
        
        
        <!-- HTML5Shiv: adds HTML5 tag support for older IE browsers -->
        <!--[if lt IE 9]>
	    <script src="js/html5shiv.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <h1>HTML Email Newsletter Testing Tool</h1>
        
		<div id="spinner"></div>
        
        
		<?php if( count(  $errors ) > 0 ): ?>
		<div id="response" class="error">
			<?php echo $errors[ 'email' ]; ?>
		</div>
		<?php elseif( strlen( $response ) > 0 ): ?>
		<div id="response" class="success">
			<?php echo $response; ?>
		</div>
		<?php endif; ?>
			 
        <form id="email-test" 
			 action="<?php echo $_SERVER['PHP_SELF']; ?>" 
			 method="post">
            <input type="text" 
                   id="email"
				   name="email"
                   placeholder="tester.joe@example.com"
				   value="<?php echo $_POST[ 'email' ]; ?>"
                   size="60" />
            <input type="submit" 
                   id="email-submit" 
                   value="Send Email" />
        </form>
    </body>
</html>
